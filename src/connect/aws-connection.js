var AWS = require("aws-sdk");
const dotenv = require('dotenv');
dotenv.config();
const config = require('config');
const region =  config.get("AWS.region")
const accessKeyId = config.get("AWS.aws_access_key_id")
const accessKeySecret = config.get("AWS.aws_secret_access_key")

AWS.config.update({"region" : region });
// @see: Updating the AWS Credentials directly
AWS.config.credentials = {
    'accessKeyId': accessKeyId,
    'secretAccessKey': accessKeySecret
}

AWS.config.getCredentials(function (err) {
    if (err) {
        console.log(err.stack);
    } else {
        console.log("Access key:", AWS.config.credentials.accessKeyId);
        console.log("Secret access key:", AWS.config.credentials.secretAccessKey);
    }
});

module.exports = AWS;
