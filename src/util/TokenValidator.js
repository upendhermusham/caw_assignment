const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dotenv = require("dotenv");
dotenv.config();
const config = require("config");
const AWS = require("../connect/aws-connection");
const jwt = require("jsonwebtoken");

async function isUserValid(req) {
    var accessToken = req.headers.authorization;
    try {
        var jwtKey = config.get("APPHASH");
        jsonValidInfo = jwt.verify(accessToken, jwtKey);
        return true;
    } catch (e) {
        console.log(e);
        if (e instanceof jwt.JsonWebTokenError) {
            console.log(e);
            return false;
        }
        return false;
    }
}
function getUserIdFromJWT(req) {
    var accessToken = req.headers.authorization;
    if (!accessToken) {
        return false;
    }
    try {
        let jwtKey = config.get("APPHASH");
        jsonValidInfo = jwt.verify(accessToken, jwtKey);
        return jsonValidInfo.userId;
    } catch (e) {
        console.log(e);
        if (e instanceof jwt.JsonWebTokenError) {
            return false;
        }

        return false;
    }
}

module.exports = { isUserValid, getUserIdFromJWT };
