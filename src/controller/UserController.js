const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
dotenv.config();
const config = require("config");
const AWS = require("../connect/aws-connection");
const UtilController = require("./UtilController");
const DocClient = new AWS.DynamoDB.DocumentClient();
var dynamoDB = new AWS.DynamoDB({ apiVersion: "2012-08-10" });
const { v4: uuidv4 } = require("uuid");
const jwt = require("jsonwebtoken");
var phoneNumber = require("awesome-phonenumber");
const axios = require("axios");

async function createUser(userData) {
    if (!(userData.id && userData.mobile && userData.country_code)) {
        return {
            success: false,
            message: "Name,Id,mobile are required to create the user",
        };
    }
    // check if user is already exists
    var isUserExists = await checkUserExists(
        userData.country_code,
        userData.mobile
    );

    if (!isUserExists.success) {
        return { success: false, message: isUserExists.message };
    }
    if (isUserExists.status) {
        return {
            success: true,
            message: "User already registered",
            id: isUserExists.id,
        };
    }

    var userParams = {
        TableName: config.get("tables.user.name"),
        Item: userData,
    };

    var creationPromise = await DocClient.put(userParams)
        .promise()
        .catch((err) => {
            return { success: false, message: err };
        });
    if (creationPromise && creationPromise.success == false) {
        return { success: false, message: creationPromise.message };
    }
    return {
        success: true,
        id: userData.id,
    };
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
async function checkUserExists(countryCode, mobile) {
    if (!mobile) {
        return { success: false, message: "mobile number is required" };
    }
    var userParams = {
        TableName: config.get("tables.user.name"),
        IndexName: config.get("tables.user.mobileIndex"),
        KeyConditionExpression: "#mobileNumber =  :mobile ",
        FilterExpression:
            "deleted_at = :emptyVal and country_code=:countryCode",
        ExpressionAttributeNames: {
            "#mobileNumber": "mobile",
        },
        ExpressionAttributeValues: {
            ":mobile": mobile,
            ":emptyVal": "",
            ":countryCode": countryCode,
        },
    };
    var data = await DocClient.query(userParams)
        .promise()
        .catch((err) => {
            return { success: false, message: err };
        });
    if (data && data.success == false) {
        return { success: false, status: false, message: data.message };
    } else {
        if (data.Items && data.Items.length) {
            if (data.Items[0].is_verified) {
                return {
                    success: true,
                    status: true,
                    message: "verified user",
                    id: data.Items[0].id,
                    data: data.Items[0],
                };
            } else {
                return {
                    success: false,
                    status: false,
                    message: "Not A Verified User",
                    data: data.Items[0],
                };
            }
        }
        return { success: true, status: false, message: "Doesn't exists" };
    }
}
async function invalidateOtps(mobile) {
    if (!mobile) {
        return { success: false, message: "Invalid mobile number" };
    }

    var otpParams = {
        TableName: config.get("tables.userOTP.name"),
        IndexName: config.get("tables.userOTP.mobileIndex"),
        KeyConditionExpression: "#mobileNumber =  :mobile ",
        FilterExpression: "deleted_at = :emptyVal",
        ExpressionAttributeNames: {
            "#mobileNumber": "mobile",
        },
        ExpressionAttributeValues: {
            ":mobile": mobile,
            ":emptyVal": "",
        },
    };
    var data = await DocClient.query(otpParams)
        .promise()
        .catch((err) => {
            return { success: false, message: err };
        });
    if (data && data.success == false) {
        return { success: false, message: data.message };
    }
    if (data && data.Items && data.Items.length) {
        var latestOtp = null;
        let currentTimeStamp = new Date().getTime();
        if (parseInt(data.Items[0].expire_time) > currentTimeStamp) {
            latestOtp = data.Items[0].otp;
        }
        for (i = 0; i < data.Items.length; i++) {
            var params = {
                TableName: config.get("tables.userOTP.name"),
                Key: {
                    id: data.Items[i].id,
                },
                UpdateExpression:
                    "set #columnName = :timeStamp, #updatedAt = :timeStamp",
                ExpressionAttributeNames: {
                    "#columnName": "deleted_at",
                    "#updatedAt": "updated_at",
                },
                ExpressionAttributeValues: {
                    ":timeStamp": new Date().getTime().toString(),
                },
                ReturnValues: "UPDATED_NEW",
            };
            var updateVal = await DocClient.update(params)
                .promise()
                .catch((err) => {
                    return { success: false, message: err };
                });
            if (updateVal.success == false) {
                return {
                    success: false,
                    message: "" + updateVal.message ? updateVal.message : "",
                };
            }
        }
        return { success: true, recentOtp: latestOtp };
    }
    return { success: true };
}
async function sendOTP(countryCode, mobile, browserInfo, deviceInfo) {
    if (!mobile || !countryCode) {
        return { success: false, message: "Invalid Mobile Number " };
    }
    if (!phoneNumber(countryCode + "" + mobile).isValid()) {
        return { success: false, message: "Invalid Mobile Number " };
    }
    var invalidateOtherOtps = await invalidateOtps(mobile);
    // Invalidating earlier otps,
    if (invalidateOtherOtps.success == false) {
        return {
            success: false,
            message:
                "failed to delete old otp: " + invalidateOtherOtps.message
                    ? invalidateOtherOtps.message
                    : "",
        };
    }
    var otp = getRandomInt(1001, 9999);
    if (invalidateOtherOtps && invalidateOtherOtps.recentOtp) {
        otp = invalidateOtherOtps.recentOtp;
    }
    var currentTimeStamp = new Date().getTime();
    var otpExpireTime = new Date().getTime() + 30 * 60 * 1000;
    var otpParams = {
        TableName: config.get("tables.userOTP.name"),
        Item: {
            id: uuidv4(),
            country_code: countryCode,
            mobile: mobile,
            otp: otp,
            expire_time: otpExpireTime,
            created_at: new Date().getTime().toString(),
            updated_at: "",
            deleted_at: "",
            browser_info: browserInfo ? browserInfo : "",
            device_info: deviceInfo ? deviceInfo : "",
        },
    };
    var creationOtp = await DocClient.put(otpParams)
        .promise()
        .catch((err) => {
            console.log(err);
            return { success: false, message: err };
        });

    if (creationOtp && creationOtp.success == false) {
        return { success: false, message: creationOtp.message };
    } else {
        var otpResponse = await UtilController.sendOTPViaSMSCBIZ(otp, mobile);
        if (otpResponse) {
            console.log("sent sms");
            return { success: true, otp: otp };
        } else {
            console.log("Failed to send otp");
            return { success: false };
        }
    }
}
async function fetchUserInfo(userId) {
    if (!userId) {
        return { success: false, message: "Invalid userId" };
    }
    var params = {
        TableName: config.get("tables.user.name"),
        Key: {
            id: userId,
        },
    };
    var userInfo = await DocClient.get(params)
        .promise()
        .catch((err) => {
            return { success: false, message: err };
        });
    if (userInfo.success == false) {
        return { success: false, message: userInfo.message };
    }
    if (userInfo && userInfo.Item) {
        if (userInfo.Item.deleted_at.length) {
            return { success: false, message: "User doesn't exists" };
        }
        return { success: true, data: userInfo.Item };
    }
    return { success: false, message: "User doesn't exists" };
}

async function validateOtp(countryCode, mobile, otp) {
    if (!(mobile && otp && countryCode)) {
        return { success: false, message: "mobile & otp are needed" };
    } else {
        var otpParams = {
            TableName: config.get("tables.userOTP.name"),
            IndexName: config.get("tables.userOTP.mobileIndex"),
            KeyConditionExpression: "#mobileNumber =  :mobile ",
            FilterExpression:
                "deleted_at = :emptyVal and #otp=:otp and country_code=:countryCode",
            ExpressionAttributeNames: {
                "#mobileNumber": "mobile",
                "#otp": "otp",
            },
            ExpressionAttributeValues: {
                ":mobile": mobile,
                ":emptyVal": "",
                ":otp": parseInt(otp),
                ":countryCode": countryCode,
            },
        };
        var data = await DocClient.query(otpParams)
            .promise()
            .catch((err) => {
                return { success: false, message: err };
            });
        if (data.success == false) {
            return { success: false, message: data.message };
        } else {
            if (data && data.Items && data.Items.length) {
                let otpExpireTime = parseInt(data.Items[0].expire_time);
                let currentTimeStamp = new Date().getTime();
                if (otpExpireTime > currentTimeStamp) {
                    var rowId = data.Items[0].id;
                    await invalidateOtps(mobile);
                    return { success: true };
                } else {
                    return { success: false, message: "Expired" };
                }
            } else {
                return { success: false, message: "Invalid otp" };
            }
        }
    }
}

async function isUserNameValid(userName) {
    if (!userName) {
        return { success: false };
    }
    // Check bad words
    var usersParams = {
        TableName: config.get("tables.user.name"),
        IndexName: config.get("tables.user.userNameIndex"),
        KeyConditionExpression: " user_name = :userName",
        FilterExpression: "#deletedAt =  :emptyVal ",
        ExpressionAttributeNames: {
            "#deletedAt": "deleted_at",
        },
        ExpressionAttributeValues: {
            ":emptyVal": "",
            ":userName": userName,
        },
    };
    var users = await DocClient.query(usersParams)
        .promise()
        .catch((err) => {
            return { success: false, message: err };
        });
    if (users.success === false) {
        return { success: false, message: users.message ? users.message : "unable validate user name" };
    }
    if (users.Items && users.Items.length) {
        return { success: false, message: users.message ? users.message : "Username is already taken" };
    } else {
        return { success: true };
    }
}
async function getUniqueInviteCode(name) {
    if (!name) {
        return { success: false };
    }
    var code = name.slice(0, 3).toLowerCase() + (Math.floor(Math.random() * 9000) + 1000);
    var unique = false;
    var errorMessage = "";

    while (true) {
        var userParams = {
            TableName: config.get("tables.user.name"),
            FilterExpression: "deleted_at = :emptyVal and invite_code=:inviteCode",
            ExpressionAttributeValues: {
                ":inviteCode": code,
                ":emptyVal": "",
            },
        };
        var data = await DocClient.scan(userParams)
            .promise()
            .catch((err) => {
                return { success: false, message: err };
            });
        if (data.success != false) {
            if (!(data.Items && data.Items.length)) {
                unique = true;
                break;
            }
        }
        if (data.success == false) {
            errorMessage = data.message ? data.message : "SOMETHING WENT WRONG";
            break;
        }
        let random = 1;
        if (code.length > 7) {
            random = parseInt(code.slice(7, code.length)) + 1;
        }

        code = code.slice(0, 7) + random;
    }
    if (unique) {
        return { success: true, data: code };
    } else {
        return { success: false, message: errorMessage };
    }
}
async function updateProfile(userId, fullName, userName, email, referralCode, source) {
    if (!userId) {
        return { success: false, message: "Invalid userId" };
    }
    if (!fullName || !userName) {
        return { success: false, message: "Name  is required" };
    }
    var inviteCode = await getUniqueInviteCode(fullName);
    if (inviteCode.success) {
        inviteCode = inviteCode.data;
    }
    var userInfo = await fetchUserInfo(userId);
    if (userInfo.success == false) {
        return { success: false, message: "Unable to fetch user data" };
    }
    var userNameValid = await isUserNameValid(userName);
    if (!userNameValid.success) {
        return { success: false, message: userNameValid.message ? userNameValid.message : "Username is already taken" };
    }
    var updateObj = {
        full_name: fullName,
        user_name: userName,
        email: email ? email : "",
        updated_at: new Date().getTime().toString(),
        invite_code: inviteCode,
        referral_code: referralCode,
        user_source: source,
    };
    updateObj = omitEmpty(updateObj);

    var updateStmt = "set ";
    var letters = ["x", "y", "z", "a", "b", "c", "d", "e", "ff", "gg", "kk", "ll", "mm"];
    var attributes = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"];
    var updateValObj = {};
    var updateAttrObj = {};
    var i = 0;
    updateObj.updated_at = new Date().getTime().toString();
    Object.keys(updateObj).forEach((key, index) => {
        if (i == 0) {
            updateStmt = updateStmt + " #" + attributes[i] + "= :" + letters[i];
        } else {
            updateStmt = updateStmt + " , #" + attributes[i] + "= :" + letters[i];
        }
        updateValObj[":" + letters[i]] = updateObj[key];
        updateAttrObj["#" + attributes[i]] = key;
        i++;
    });
    var params = {
        TableName: config.get("tables.user.name"),
        Key: {
            id: userId,
        },
        UpdateExpression: updateStmt,
        ExpressionAttributeNames: updateAttrObj,
        ExpressionAttributeValues: updateValObj,
        ReturnValues: "UPDATED_NEW",
    };
    var updateVal = await DocClient.update(params)
        .promise()
        .catch((err) => {
            return { success: false, message: err };
        });
    if (updateVal.success == false) {
        return { success: false, message: updateVal.message };
    } else {
        return { success: true };
    }
}
module.exports = {
    sendOTP,
    validateOtp,
    createUser,
    fetchUserInfo,
    updateProfile
};
