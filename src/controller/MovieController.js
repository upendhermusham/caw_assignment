const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
dotenv.config();
const config = require("config");
const AWS = require("../connect/aws-connection");
const DocClient = new AWS.DynamoDB.DocumentClient();
var dynamoDB = new AWS.DynamoDB({ apiVersion: "2012-08-10" });
const { v4: uuidv4 } = require("uuid");
const jwt = require("jsonwebtoken");
var phoneNumber = require("awesome-phonenumber");
const { upperFirst, isArray } = require("lodash");

async function fetchTheatresForCity(city) {
    if (!city) {
        return {
            success: false,
            message: "City is required to fetch the theatres",
        };
    }
    city = city.toLowerCase();
    var theatreParams = {
        TableName: config.get("tables.theatre.name"),
        IndexName: config.get("tables.theatre.cityIndex"),
        ProjectionExpression: "#id",
        KeyConditionExpression: "#city =  :city",
        FilterExpression: "deleted_at = :emptyVal and is_open=:trueVal",
        ExpressionAttributeNames: {
            "#city": "city",
            "#id": "id",
        },
        ExpressionAttributeValues: {
            ":city": city,
            ":emptyVal": "",
            ":trueVal": true,
        },
    };
    var movieData = await DocClient.query(theatreParams)
        .promise()
        .catch((err) => {
            return { success: false, message: err };
        });
    if (movieData.success === false) {
        return {
            success: false,
            message: movieData.message
                ? movieData.message
                : "Failed to fetch data",
        };
    }

    let theatreIds = movieData.Items.map((movie) => movie.id);
    return { success: true, data: theatreIds };
}
async function fetchMoviesForTheatres(theatresIds) {
    if (theatresIds.length === 0) {
        return { success: true, data: [] };
    }
    var movieIdsStatus = await fetchMovieIdsForTheatres(theatresIds);
    if (movieIdsStatus.success) {
        var movieTable = config.get("tables.movie.name");
        var data = [];
        movieIds = movieIdsStatus.data;
        for (i = 0; i < movieIds.length; ) {
            var keys = [];
            nextSet = 100;
            if (i + nextSet > movieIds.length) {
                nextSet = movieIds.length - i;
            }

            for (j = i; j < i + nextSet; j++) {
                keys.push({
                    id: movieIds[j],
                });
            }

            var params = {
                RequestItems: {
                    [movieTable]: {
                        Keys: keys,
                    },
                },
            };
            var moviesInfo = await DocClient.batchGet(params)
                .promise()
                .catch((err) => {
                    {
                        console.log(err);
                        return { success: false, message: err };
                    }
                });
            if (moviesInfo.success === false) {
                return {
                    success: false,
                    message: moviesInfo.message
                        ? moviesInfo.message
                        : "Failed to fetch users info",
                };
            }
            data = data.concat(moviesInfo.Responses[movieTable]);
            if (i >= movieIds.length) {
                break;
            }
            i = i + nextSet;
        }
        return { success: true, data: data };
    } else {
        return { success: false, message: "Failed to fetch the data" };
    }
}
async function fetchMovieIdsForTheatres(theatreIds) {
    if (theatreIds.length === 0) {
        return { success: true, data: [] };
    }
    console.log(JSON.stringify(theatreIds));
    var movieIds = [];
    for (i = 0; i < theatreIds.length; i++) {
        var screeningParams = {
            TableName: config.get("tables.screening.name"),
            IndexName: config.get("tables.screening.theatreIndex"),
            ProjectionExpression: "movie_id",
            KeyConditionExpression: "#theatreId =  :theatreId",
            FilterExpression: "deleted_at = :emptyVal and is_ended <> :trueVal",
            ExpressionAttributeNames: {
                "#theatreId": "theatre_id",
            },
            ExpressionAttributeValues: {
                ":emptyVal": "",
                ":trueVal": true,
                ":theatreId": theatreIds[i].toString(),
            },
        };
        var movieData = await DocClient.query(screeningParams)
            .promise()
            .catch((err) => {
                console.log(err);
                return { success: false, message: err };
            });
        if (movieData.success != false) {
            let ids = movieData.Items.map((screening) => screening.movie_id);
            movieIds = movieIds.concat(ids);
        }
    }
    uniqueMovieIds = movieIds.filter(function (item, pos) {
        return movieIds.indexOf(item) == pos;
    });
    return { success: true, data: uniqueMovieIds };
}
async function fetchTheatresForMovie(movieId) {
    if (!movieId) {
        return {
            success: false,
            message: "Movie Id is required to fetch the screenings",
        };
    }
    var screeningParams = {
        TableName: config.get("tables.screening.name"),
        IndexName: config.get("tables.screening.movieIndex"),
        KeyConditionExpression: "#movieId =  :movieId",
        FilterExpression: "deleted_at = :emptyVal and is_ended <> :trueVal",
        ExpressionAttributeNames: {
            "#movieId": "movie_id",
        },
        ExpressionAttributeValues: {
            ":emptyVal": "",
            ":trueVal": true,
            ":movieId": movieId.toString(),
        },
    };
    var screeningData = await DocClient.query(screeningParams)
        .promise()
        .catch((err) => {
            console.log(err);
            return { success: false, message: err };
        });
    if (screeningData.success == false) {
        return {
            success: false,
            message: screeningData.message ? screeningData.message : "",
        };
    }
    let theatreIds = screeningData.Items.map(
        (screening) => screening.theatre_id
    );
    var theatreInfo = await fetchTheatreProfile(theatreIds);
    if (theatreInfo.success) {
        for (i = 0; i < screeningData.Items.length; i++) {
            let theatreData = theatreInfo.data.find((theatre) => {
                return theatre.id === screeningData.Items[i].theatre_id;
            });
            screeningData.Items[i].profile = theatreData;
            // Remove unnecessary details
            delete screeningData.Items[i].movie_id;
            delete screeningData.Items[i].updated_at;
            delete screeningData.Items[i].created_at;
            delete screeningData.Items[i].is_ended;
        }
    }

    return { success: true, data: screeningData.Items };
}
async function fetchTheatreProfile(theatreIds = []) {
    if (theatreIds.length == 0) {
        return { success: false, message: "empty theatres ids received" };
    }
    // Removing duplicates
    theatreIds = theatreIds.filter(function (item, pos) {
        return theatreIds.indexOf(item) == pos;
    });

    var theatreTable = config.get("tables.theatre.name");
    var data = [];

    for (i = 0; i < theatreIds.length; ) {
        var keys = [];
        nextSet = 100;
        if (i + nextSet > theatreIds.length) {
            nextSet = theatreIds.length - i;
        }

        for (j = i; j < i + nextSet; j++) {
            keys.push({
                id: theatreIds[j],
            });
        }

        var params = {
            RequestItems: {
                [theatreTable]: {
                    Keys: keys,
                },
            },
        };
        var theatreInfo = await DocClient.batchGet(params)
            .promise()
            .catch((err) => {
                {
                    console.log(err);
                    return { success: false, message: err };
                }
            });
        if (theatreInfo.success === false) {
            return {
                success: false,
                message: theatreInfo.message
                    ? theatreInfo.message
                    : "Failed to fetch users info",
            };
        }
        data = data.concat(theatreInfo.Responses[theatreTable]);
        if (i >= theatreIds.length) {
            break;
        }
        i = i + nextSet;
    }
    return { success: true, data: data };
}
async function fetchScreeningsForTheatre(theatreId) {
    if (!theatreId) {
        return {
            success: false,
            message: "Theatre Id is mandatory to fetch the data",
        };
    }
    var screeningParams = {
        TableName: config.get("tables.screening.name"),
        IndexName: config.get("tables.screening.theatreIndex"),
        ProjectionExpression: "#id",
        KeyConditionExpression: "#theatreId =  :theatreId",
        FilterExpression: "deleted_at = :emptyVal and is_ended <> :trueVal",
        ExpressionAttributeNames: {
            "#theatreId": "theatre_id",
            "#id": "id",
        },
        ExpressionAttributeValues: {
            ":emptyVal": "",
            ":trueVal": true,
            ":theatreId": theatreId.toString(),
        },
    };
    var screeningData = await DocClient.query(screeningParams)
        .promise()
        .catch((err) => {
            console.log(err);
            return { success: false, message: err };
        });
    if (screeningData.success == false) {
        return {
            success: false,
            message: screeningData.message
                ? screeningData.message
                : "Something went wrong in fetching the screens",
        };
    }
    var theatreProfile = await fetchTheatreProfile([theatreId]);

    var theatreSeats = Object.values(theatreProfile.data[0].seats)[1];
    if (theatreSeats.length) {
        for (i = 0; i < screeningData.Items.length; i++) {
            var bookedSeats = await fetchBookingsForScreening(
                screeningData.Items[i].id
            );
            if (bookedSeats.success) {
                var availableSeats = theatreSeats.filter(
                    (x) => !bookedSeats.data.includes(x)
                );
                screeningData.Items[i].available_seats = availableSeats;
            }
        }
        return {
            success: true,
            data: screeningData.Items,
        };
    } else {
        return {
            success: true,
            data: [],
        };
    }
}
async function fetchBookingsForScreening(screeningId) {
    if (!screeningId) {
        return { success: false, message: "invalid screening id" };
    }

    var screeningParams = {
        TableName: config.get("tables.bookings.name"),
        IndexName: config.get("tables.bookings.screeningIndex"),
        KeyConditionExpression: "#screeningId =  :screeningId",
        FilterExpression:
            "deleted_at = :emptyVal and booking_status in (:bookedVal,:pendingVal)",
        ExpressionAttributeNames: {
            "#screeningId": "screening_id",
        },
        ExpressionAttributeValues: {
            ":emptyVal": "",
            ":screeningId": screeningId.toString(),
            ":bookedVal": "booked",
            ":pendingVal": "pending",
        },
    };
    var bookings = await DocClient.query(screeningParams)
        .promise()
        .catch((err) => {
            console.log(err);
            return { success: false, message: err };
        });
    if (bookings.success === false) {
        return {
            success: false,
            message:
                "Failed to fetch the data:" + bookings.message
                    ? bookings.message
                    : "",
        };
    }
    var bookedSeatsArray = [];

    bookings.Items.map((item) => {
        bookedSeatsArray.push(item.seat);
    });
    return { success: true, data: bookedSeatsArray };
}
async function isSeatValidAndEmpty(screeningId, seat) {
    if (!screeningId) {
        return {
            success: false,
            message: "Screening Id is mandatory to validate the seat",
        };
    }
    var screeningParams = {
        TableName: config.get("tables.screening.name"),
        Key: {
            id: screeningId,
        },
    };
    var screeningInfo = await DocClient.get(screeningParams)
        .promise()
        .catch((err) => {
            return { success: false, message: err };
        });
    if(screeningInfo.success === false)
    {
        return({success:false,message:screeningInfo.message?screeningInfo.message:""})
    }
    
    if(!(screeningInfo.Item && Object.keys(screeningInfo.Item).length))
    {
        return({success:false,message:"Invalid screening Id"})
    }
    let theatreId  = screeningInfo.Item.theatre_id;
    if(!theatreId)
    {
        return({success:false,message:"Theatre info is missing in screening"}) 
    }
    var theatreInfo = await fetchTheatreProfile([theatreId]);
    if (!theatreInfo.success) {
        return {
            success: false,
            message: "failed to fetch theatre data",
        };
    }
    var theatreSeats = Object.values(theatreInfo.data[0].seats)[1];
    if (theatreSeats.includes(seat)) {
        var bookedSeats = await fetchBookingsForScreening(screeningId);
        if (bookedSeats.data.includes(seat)) {
            return {
                success: false,
                message: "Seat is already booked",
            };
        }
        return {
            success: true,
            message: "Valid and empty seat",
        };
    } else {
        return {
            success: false,
            message: "Invalid Seat",
        };
    }
}
async function bookSeat(seatObjVal) {
    var bookingParams = {
        TableName: config.get("tables.bookings.name"),
        Item: seatObjVal,
    };
    var bookingStatus = await DocClient.put(bookingParams)
        .promise()
        .catch((err) => {
            return { success: false, message: err };
        });
    if (bookingStatus.success === false) {
        return {
            success: false,
            message: bookingStatus.message
                ? bookingStatus.message
                : "Failed to book seat",
        };
    } else {
        return { success: true };
    }
}
module.exports = {
    fetchTheatresForCity,
    fetchMoviesForTheatres,
    fetchTheatresForMovie,
    fetchScreeningsForTheatre,
    isSeatValidAndEmpty,
    bookSeat
};
