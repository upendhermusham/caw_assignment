const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
dotenv.config();
const config = require("config");
const AWS = require("../connect/aws-connection");
const DocClient = new AWS.DynamoDB.DocumentClient();
const axios = require("axios");
var request = require("request");
const fastSmsAPIKey = config.get("FAST_2_SMS_API_KEY");


async function sendOTPViaSMSCBIZ(otp, number) {
    console.log("VALIDATE OTP SMS");
    console.log(otp + ":otp");
    var url =
        "https://www.fast2sms.com/dev/bulk?authorization=" +
        fastSmsAPIKey +
        "&sender_id=SMSINI&language=english&route=qt&numbers=" +
        number +
        "&message=40258&variables={BB}&variables_values=" +
        otp;
    var msgStatus = true;
    try {
        var response = await axios.get(url);
        console.log("SMS response:" + response.status);
        return true;
    } catch (e) {
        console.log("SMS error" + e);
        return false;
    }

}


module.exports = {  sendOTPViaSMSCBIZ};
