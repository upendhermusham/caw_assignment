const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dotenv = require("dotenv");
dotenv.config();
const config = require("config");
const AWS = require("./src/connect/aws-connection");
const TokenValidator = require("./src/util/TokenValidator");

// Routes Definitions
const movieRoutes = require("./routes/movie");
const userRoutes = require("./routes/user");

// App Start
var app = express();

// Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Enable cors
app.use(
    cors({
        origin: "*",
        methods: "*",
        allowedHeaders: "*",
        optionsSuccessStatus: 200,
    })
);
var validateToken = async function (req, res, next) {
    console.log("Request Path:" + req.path);
    if (req.path.includes("public/")) {
        return next();
    } 
    // user paths required login
    else {
        var isTokenValid = await TokenValidator.isUserValid(req);
        if (isTokenValid) {
            next();
        } else {
            res.status(403).send({ success: "false", message: "Unauthorized Access" });
        }
    }
};

app.use(validateToken);
app.use("/public/", movieRoutes);
app.use("/user/", userRoutes);

//  module.exports = app;
app.listen(config.get("PORT"));
