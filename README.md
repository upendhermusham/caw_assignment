# CAW Assignment

It is a colletion of APIs which are needed for ticket booking and fetching movie/theatre/showtime information.

## Stack Used
1. Database  : AWS DynamoDB
2. Hosted at : AWS Lambda (serverless)
3. Framework : Node JS
4. Logging   : AWS CloudWatch

## Installation

1. Clone the project source code from bitbucket repo
```bash
git clone https://bitbucket.org/upendhermusham/caw_assignment.git
```
2. Install packages
```bash
npm install
```

 
## Usage

1. Run local server
```bash
npm run serve
```


## Notes
1. This project is hosted as lambda function in AWS, which can be accessed at https://4h0scgm40l.execute-api.ap-south-1.amazonaws.com/prod/
2. I have used few commercial third party services ( SMS,AWS). So before running in local make sure to give the proper keys in config file


## Author
Developed By Upendher Musham
```bash
  Checkout more about me at http://upendhermusham.96.lt/
```