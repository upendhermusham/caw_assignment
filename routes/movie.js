const express = require("express");
const Router = express.Router();
const AWS = require("../src/connect/aws-connection");
const MovieController = require("../src/controller/MovieController");

Router.get("/:city/movies", async (req, res) => {
    var city = req.params.city;
    if (!city) {
        res.status(403).send({
            success: false,
            message: "City name is mandatory to fetch data",
        });
        return 0;
    }
    var theatres = await MovieController.fetchTheatresForCity(city);
    if (theatres.success) {
        var moviesList = await MovieController.fetchMoviesForTheatres(
            theatres.data
        );
        if (moviesList.success) {
            res.status(200).send({ success: true, data: moviesList.data });
            return 0;
        }
        res.status(500).send({
            success: false,
            message:
                "Failed to fetch the data, Please try again ," +
                moviesList.message
                    ? moviesList.message
                    : "",
        });
    } else {
        res.status(500).send({
            success: false,
            message:
                "Failed to fetch the data, Please try again ," +
                theatres.message
                    ? theatres.message
                    : "",
        });
    }
});
Router.get("/movie/:id", async (req, res) => {
    var movieId = req.params.id;
    if (!movieId) {
        res.status(403).send({
            success: false,
            message: "Movie id is mandatory to fetch data",
        });
        return 0;
    }
    var theatresInfo = await MovieController.fetchTheatresForMovie(movieId);
    if (theatresInfo.success) {
        res.status(200).send({ success: true, data: theatresInfo.data });
        return 0;
    } else {
        res.status(500).send({
            success: false,
            message:
                "Failed to fetch the data, Please try again ," +
               theatresInfo.message
                    ?theatresInfo.message
                    : "",
        });
    }
});
Router.get("/theatre/:id", async (req, res) => {
    var theatreId = req.params.id;
    if (!theatreId) {
        res.status(403).send({
            success: false,
            message: "Movie id is mandatory to fetch data",
        });
        return 0;
    }
    var screeningInfoWithAvailableSeats = await MovieController.fetchScreeningsForTheatre(theatreId);
    if (screeningInfoWithAvailableSeats.success) {
        
            res.status(200).send({ success: true, data: screeningInfoWithAvailableSeats.data });
            return 0;
      
       
    } else {
        res.status(500).send({
            success: false,
            message:
                "Failed to fetch the data, Please try again ," +
               screeningInfoWithAvailableSeats.message
                    ?screeningInfoWithAvailableSeats.message
                    : "",
        });
    }
});

module.exports = Router;
