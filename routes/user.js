const express = require("express");
const bodyParser = require("body-parser");
const Router = express.Router();
const dotenv = require("dotenv");
dotenv.config();
const config = require("config");
const AWS = require("../src/connect/aws-connection");
const DocClient = new AWS.DynamoDB.DocumentClient();
const jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");
var phoneNumber = require("awesome-phonenumber");
var validate = require("validate.js");
const crypto = require("crypto");
const UserController = require("../src/controller/UserController");
const TokenValidator = require("../src/util/TokenValidator");
const MovieController = require("../src/controller/MovieController");


Router.post("/sign-in", async (req, res) => {
    var mobile = req.body.mobile;
    var countryCode = "+91";
    var browserInfo = req.body.browserInfo;
    var deviceInfo = req.body.deviceInfo;
    if (!mobile) {
        res.status(401).send({
            success: false,
            message: "Kindly enter mobile number",
        });
        return 0;
    }
    if (!phoneNumber(countryCode + "" + mobile).isValid()) {
        res.status(200).send({
            success: false,
            message: "Invalid Mobile Number",
        });
        return 0;
    }

    // @todo: validate mobile number
    var sendOtp = await UserController.sendOTP(countryCode, mobile, browserInfo, deviceInfo);
    if (sendOtp.success) {
        res.status(200).send({ success: true, message: "OTP has been sent" });
    } else {
        res.status(500).send({
            success: false,
            message: "Failed to send OTP:" + sendOtp.message ? sendOtp.message : "",
        });
    }
});

Router.post("/validate-otp", async (req, res) => {

    var mobile = req.body.mobile;
    var countryCode = "+91";
    var browserInfo = req.body.browserInfo;
    var deviceInfo = req.body.deviceInfo;
    var otp = req.body.otp;
    if (!(mobile)) {
        res.status(401).send({
            success: false,
            message: "Kindly enter mobile number",
        });
        return 0;
    }
    if (!(otp)) {
        res.status(401).send({
            success: false,
            message: "Kindly enter otp",
        });
        return 0;
    }
    if (!phoneNumber(countryCode + "" + mobile).isValid()) {
        res.status(200).send({
            success: false,
            message: "Invalid Mobile Number",
        });
        return 0;
    }
    // Validate otp against mobile number, if it is valid check user exists or not, if not exists create new one  if exists log
    var isOtpValid = await UserController.validateOtp(countryCode, mobile, otp);
    if (isOtpValid && isOtpValid.success) {
        var userId = uuidv4();
        var userData = {
            mobile: mobile,
            country_code: countryCode,
            id: userId,
            created_at: new Date().getTime().toString(),
            updated_at: "",
            deleted_at: "",
            is_verified: true          
        };
    
        var userProfile = await UserController.createUser(userData);
        if (userProfile.success) {
            var userDetails = await UserController.fetchUserInfo(userProfile.id);
            var userInfo = {};
            if (userDetails.success && Object.keys(userDetails.data).length) {
                userId = userProfile.id;
                userInfo = userDetails.data;
            }
            var jwtKey = config.get("APPHASH");
            var jwtExpirySeconds = config.get("JWT_EXPIRE_TIME");
            var jwtToken = jwt.sign({ userId }, jwtKey, {
                algorithm: "HS256",
                expiresIn: jwtExpirySeconds,
            });

            res.status(200).send({
                success: true,
                token: jwtToken,
                userInfo: userInfo,
            });
        } else {
            res.status(200).send({
                success: false,
                message: createUser.message ? createUser.message : "Failed to validate",
            });
        }
    } else {
        res.status(200).send({ success: false, message: "invalid otp" });
    }
});
Router.post("/update-profile", async (req, res) => {
    var userId = TokenValidator.getUserIdFromJWT(req);
    if(!userId)
    {
        res.status(403).send({
            success:false,
            message:"Unathorized Access"
        })
    }
    var fullName = req.body.fullName;
    var userName = req.body.userName;
    var email = req.body.email;
    var referalCode = req.body.referralCode;
    var source = req.body.source;

    if (!fullName || !userName ) {
        res.status(200).send({ success: false, message: "Full name & Username are mandatory values to update " });
    } else {
        if (email && email.length) {
            let emailError = validate.single(email, {
                presence: true,
                email: true,
            });
            if (emailError && emailError.length) {
                res.status(200).send({
                    success: false,
                    message: "Invalid Email",
                });
                return 0;
            }
        }

        var format = /[^\w\s]/gi;
       
        let numberRegex = /\d/g;
        if (format.test(fullName) || numberRegex.test(fullName)) {
            res.status(200).send({
                success: false,
                message: "Kindly provide valid fullName without any special characters",
            });
            return 0;
        }
        if(fullName.length<3)
        {
            res.status(200).send({
                success: false,
                message: "Kindly provide full name with minimum characters 3",
            });
            return 0;
        }
        if (format.test(userName)) {
            res.status(200).send({
                success: false,
                message: "Kindly provide valid User name without any special characters",
            });
            return 0;
        }
        if(userName.length<3)
        {
            res.status(200).send({
                success: false,
                message: "Kindly provide user name with minimum characters 3",
            });
            return 0;
        }
        if (/\s/.test(userName)) {
            res.status(200).send({
                success: false,
                message: "Kindly choose another username, spaces are not allowed",
            });
            return 0;
        }
       
       
        
        if (source && source.length) {
            if (source.length < 4) {
                res.status(200).send({
                    success: false,
                    message: "Text entered regarding where have you heard about us should be min. 4 characters long.",
                });
                return 0;
            }
            if (format.test(source)) {
                res.status(200).send({
                    success: false,
                    message: "Text entered regarding where have you heard about us should not contain any special characters.",
                });
                return 0;
            }
        }

        if (referalCode && referalCode.length) {
            if (format.test(referalCode)) {
                res.status(200).send({
                    success: false,
                    message: "Kindly provide valid code",
                });
                return 0;
            }
        }
        var userInfo = {};
        var updatingProfile = await UserController.updateProfile(userId, fullName, userName, email, referalCode, source);
        if (updatingProfile.success) {
            var userDetails = await UserController.fetchUserInfo(userId);
            if (userDetails.success) {
                userInfo = userDetails.data;
            }

            res.status(200).send({
                success: true,
                message: "profile has been saved",
                userInfo: userInfo,
            });
        } else {
            res.status(200).send({
                success: false,
                message: updatingProfile.message,
            });
        }
    }
});

Router.post("/book", async (req, res) => {
    var screeningId = req.body.screeningId;
    var seat = req.body.seat
    var userId = TokenValidator.getUserIdFromJWT(req);
    if(!screeningId || !seat)
    {
        res.status(401).send({
            success:false,
            message: "screening Id & seat is mandatory to book ticket"
        })
        return 0;
    }
    var seatValidity = await MovieController.isSeatValidAndEmpty(screeningId,seat);
    if(seatValidity.success)
    {
        let ticketId = uuidv4()
        var seatObjVal = {
            id : ticketId,
            user_id : userId,
            screening_id : screeningId,
            seat : seat,
            booking_status : "booked",
            payment_details : {
                payment_method : "NA",
                price : "0"
            },
            created_at : new Date().getTime(),
            updated_at : "",
            deleted_at : ""
        }
        var bookingStatus = await MovieController.bookSeat(seatObjVal);
        if(bookingStatus.success)
        {
            res.status(200).send({
                success: true,
                ticketId : ticketId
            })
            return 0;
        }
        else{
            res.status(500).send({
                success:false,
                message:"Failed to book ticket, any money deducted will be refunded in 2-3 business days"+ bookingStatus.message?bookingStatus.message:""
            })
            return 0
        }
    }
    res.status(500).send({
        success:false,
        message: seatValidity.message?seatValidity.message:""
    })

})

module.exports = Router;
